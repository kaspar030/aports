# Contributor: Mike Crute <mike@crute.us>
# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=tiny-cloud
pkgver=3.1.0_rc1
pkgrel=0
pkgdesc="Tiny Cloud instance bootstrapper"
url="https://gitlab.alpinelinux.org/alpine/cloud/tiny-cloud"
arch="noarch"
license="MIT"
checkdepends="kyua xz lz4 zstd"
depends="e2fsprogs-extra partx sfdisk yx openssh-server"
provides="tiny-cloud-allclouds=$pkgver-r$pkgrel"
source="
	$url/-/archive/$pkgver/tiny-cloud-$pkgver.tar.gz
	tiny-cloud.pre-upgrade
	tiny-cloud-openrc.pre-upgrade
	tiny-cloud-aws.post-install
	tiny-cloud-azure.post-install
	tiny-cloud-gcp.post-install
	tiny-cloud-hetzner.post-install
	tiny-cloud-incus.post-install
	tiny-cloud-nocloud.post-install
	tiny-cloud-oci.post-install
	tiny-cloud-scaleway.post-install
"
subpackages="
	$pkgname-openrc
	$pkgname-aws
	$pkgname-azure
	$pkgname-gcp
	$pkgname-hetzner
	$pkgname-incus
	$pkgname-nocloud
	$pkgname-oci
	$pkgname-scaleway
"

check() {
	make check
}

package() {
	make PREFIX="$pkgdir" core openrc
	install="$pkgname.pre-upgrade"
}

openrc() {
	install="$pkgname-openrc.pre-upgrade"
	default_openrc
}

aws() {
	pkgdesc="Tiny Cloud - Amazon Web Services module"
	depends="$pkgname=$pkgver-r$pkgrel"
	provides="tiny-ec2-bootstrap"
	install="$pkgname-aws.post-install"
	mkdir -p "$subpkgdir"
}

azure() {
	pkgdesc="Tiny Cloud - Azure module"
	depends="$pkgname=$pkgver-r$pkgrel"
	install="$pkgname-azure.post-install"
	mkdir -p "$subpkgdir"
}

gcp() {
	pkgdesc="Tiny Cloud - Google Cloud Platform module"
	depends="$pkgname=$pkgver-r$pkgrel"
	install="$pkgname-gcp.post-install"
	mkdir -p "$subpkgdir"
}

hetzner() {
	pkgdesc="Tiny Cloud - Hetzner Cloud module"
	depends="$pkgname=$pkgver-r$pkgrel"
	install="$pkgname-hetzner.post-install"
	mkdir -p "$subpkgdir"
}

incus() {
	pkgdesc="Tiny Cloud - Incus module"
	depends="$pkgname=$pkgver-r$pkgrel"
	install="$pkgname-incus.post-install"
	mkdir -p "$subpkgdir"
}

nocloud() {
	pkgdesc="Tiny Cloud - NoCloud module"
	depends="$pkgname=$pkgver-r$pkgrel"
	provides="tiny-cloud-alpine=$pkgver-r$pkgrel"
	install="$pkgname-nocloud.post-install"
	mkdir -p "$subpkgdir"
}

oci() {
	pkgdesc="Tiny Cloud - Oracle Cloud Infrastructure module"
	depends="$pkgname=$pkgver-r$pkgrel"
	install="$pkgname-oci.post-install"
	mkdir -p "$subpkgdir"
}

scaleway() {
	pkgdesc="Tiny Cloud - Scaleway Cloud module"
	depends="$pkgname=$pkgver-r$pkgrel"
	install="$pkgname-scaleway.post-install"
	mkdir -p "$subpkgdir"
}

sha512sums="
178fa84a03a610c2a2fff331b79e5c385a0509f33cd335f172fdea5a163ae0e8df1b98b7440909de8ad09ff3f726c5df464202aa748eb6d39426e9348085b311  tiny-cloud-3.1.0_rc1.tar.gz
83217a30e495bcc18ad1a5d744806d499b5bdf929df3f18597216a25f465e5d4764ca66499d221bf5738b83639f1ec80de2a14e4b64aac80d51b285c01f0fc74  tiny-cloud.pre-upgrade
a2f795c1d9cce1a796cc92711b18459706b6573417d2ef43504c50a65f4808eaf8f8d48a20a4f198ade25c285188659a95d51e5cb34e4c64cf6384459397b850  tiny-cloud-openrc.pre-upgrade
c1c36e91cb7da288661e1ef2726869059211c48d7f2045824942fb4c107d397586009e2d65e8e1b03107c9d5fa46b0857b306d1e639b292aed5fe375f6865582  tiny-cloud-aws.post-install
49f47bcf72932c26c71151be8e1a78c37bba72db2c47e97a481e6727d892eea7881ee2e5bb976958b963908590eecd056590e7bde68fb5b77a99c39784576a75  tiny-cloud-azure.post-install
94ef1b7acee757e88e820a274d095e726dc0fb3cb925e347e6bc1f48d89cfe3cbff929235af2f445ffb3fde23b354e01a521dd09157f618f650e24f57562b45f  tiny-cloud-gcp.post-install
1cc8b22e95a7ff8bd8639598fea94347e3f1a05451bb4d555400cdac7ed0e91c579b31db5cad1c3b28eb9a61b983085936b3537684a93b94c39a5ab49a606812  tiny-cloud-hetzner.post-install
ad43f362d79c7ce10c83393998c67480360493dc5c5483dedf721c114364086257b1c1b957800a13e517b1698203bc9b6491ac3124a99d1c2b73d845aa3c26cc  tiny-cloud-incus.post-install
b435076c4463563f5e30a2c88278fb93eebaa7c0c39d156b7596ece4a2116bdb5a852bf91e40573b04d357eb34b9a1600193d46a76916ddd4be337270b2d599e  tiny-cloud-nocloud.post-install
97ff457e1c37b1bcec6c5ba821dd7fc9788a3e0dae5a43cd5ee3dd216db3a9255c5d43c6472ad7b094f2155f490b312c49a2a18facc8935eb2c3413f011d56e8  tiny-cloud-oci.post-install
fd944b9a2a6203045699e42cb1a9016f7a7ca61003ff6f256a2e9075bfe2bdb81c2c822ed3bf695199a84a0f2cba13418317c8c0c81e2975c6ccf33d111382fe  tiny-cloud-scaleway.post-install
"
